package com.hcl.webinar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.webinar.model.Login;

@Controller
public class LoginController {

    @RequestMapping("/")
    public ModelAndView viewHome(@ModelAttribute("login") Login login) {
        return new ModelAndView("login");
    }
}
