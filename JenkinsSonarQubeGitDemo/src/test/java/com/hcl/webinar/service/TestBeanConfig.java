package com.hcl.webinar.service;


import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = "com.hcl.webinar")
public class TestBeanConfig {

}